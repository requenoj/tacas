\section{Motivation}

Our initial motivation comes from \emph{multi-criteria optimization} where $X$ is the cost space of the optimization problem. For minimization,  $\uX$ corresponds to \emph{infeasible} costs and $\oX$ represents the \emph{feasible} costs, while for maximization it is the other way around. The solution of a multi-criteria optimization problem is the set $\min (\oX)$, also known as the \emph{Pareto front} of the problem,  or its approximation by $\min (\oY)$. In \cite{julien}  we developed a procedure for computing such an approximation using a variant of binary search that submits queries to a constraint solver concerning the existence of solutions of a given cost $x$. The costs used in the queries were selected in order to reduce the distance between $\oY$ and $\uY$ and improve approximation quality. Note, however, that we do not know a priori whether $x$ is feasible or not, and the influence of the answer on the distance between $\uY$ and $\oY$ is different in the two cases.

\ni{\bf Remark}:  The set of costs associated with actual feasible solutions to combinatorial and mixed optimization problems can be a \emph{discrete} subset of $X$ and the upward closure property of $\oX$ is obtained by assuming that you can always pay more for the same solution. Hence the algorithm described in the sequel using a continuous terminology should be slightly modified to solve such optimization problems practically. In particular  a query concerning the membership of $x$ in $\oX$ may return a positive answer about some $x'<x$. We also postpone the treatment of practically non-terminating queries that may occur in hard problems as $x$ gets closer to the boundary between the feasible and the infeasible.

Another motivation comes from parametric identification where we have a parameterized family of predicates/constraints $\{\f_p\}$ where $p$ is a vector of parameters ranging over some rectangle. Given one or more instances of objects from the domain of the predicates, we would like to know the range of parameters $p$ such that $\f_p$ holds on those instances.  As an example consider a real-valued signal $u(t)$ and a temporal formula of the form $\exists t<p_1 ~ u(t)<p_2$. It is not hard to see that for any signal, the set of parameters $p=(p_1,p_2)$ that render $\f_p$ satisfied is upward closed. In general, the problem can be trivially reduced to monotone partition approximation if the influence of each parameter on satisfaction is monotone  \cite{parametric}.

It is worth noting that the problem we solve differs from some other work that goes by the name of multi-dimensional binary search, where one searches for a \emph{single point} while we want to approximate a \emph{surface} of a dimension $(d-1)$.

\section{One Dimension}

Below we formulate classical binary search in terms that will facilitate the extension to many dimensions.
 When $d=1$, $X$ is a bounded interval that we assume without loss of generality to be $[0,1]$, $\uX=[0,p)$ and $\oX=[p,1]$ for some $0<p<1$. The outcome of the binary search procedure is an interval $[a,b]$ containing $p$ such that $([0,a),[b,1]))$ is an approximation of $([0,p),[p,1])$. In dimension $1,$ two styles of measuring the quality of the approximation coincide: $b-a$ is at the same time the ``volume" of the uncertainty set and its ``diameter", which is the Hausdorff distance between $\uY$ and $\oY$.

 The binary search procedure is shown in Algorithm~\ref{alg:binary}. It uses an oracle for the membership of a point $x$ in $\uX$ or $\oX$. Figure~\ref{fig:search-tree} illustrates a run of the algorithm. Binary search can be viewed also as selecting a path in a binary tree whose nodes are labeled by intervals, with the root labeled by $X$ and the sons of each interval $[a,b]$ are the intervals $[a,(a+b)/2]$ and $[(a+b)/2, b]$.

\begin{algorithm}
  \caption{One dimensional binary search}
  \label{alg:binary}
  \begin{algorithmic}[1]
    \State {{\bf Input}: An interval $[a,b]$ such that $a\in \uX$ and $b\in \oX$.}
     \State {{\bf Output}: An interval $[a',b']$ such that  $a'\in \uX$, $b'\in \oX$ and $b'-a'<\epsilon$.}
     \smallskip
    \While {$b-a>\epsilon$}
        \State {$q=(a+b)/2$}
      \If {$q\in \uX$}
          \State   {$[a,b]=[q,b]$}
      \Else
              \State   {$[a,b]=[a,q]$}
      \EndIf
          \EndWhile
          \State {$[a',b']=[a,b]$}
  \end{algorithmic}
\end{algorithm}

%\begin{figure}[h]
%  \centering
%  \input search-tree.PDFTEX_T
%  \caption{Binary search and the successive reduction of the uncertainty interval. The rightmost known element of $\uX$ and leftmost known elements of $\oX$ are indicated by $-$ and $+$, respectively. On the right we see the same process interpreted as path selection in a binary tree.}\label{fig:search-tree}
%\end{figure}


.\ft{This idea, I feel, is somehow related to the practice of reducing multi-criteria to single-criterion by taking a convex combination of the costs.}




\section{Going Multi-Dimensional}

%
Concerning the complexity of the algorithm, for $d=2$, applying $search()$  to a rectangle of diagonal length $c$ involves $\log (c/\eps)$ queries and this becomes negligible as $c$ decreases during the process. Typically, when we apply this basic step to an uncertainty rectangle of volume $V$, we divide that volume by $2$.  Consider now a breadth-first execution of the algorithm. At the end of level $i$ we have invoked the $search()$ procedure $2^i$ times and divided the volume by $2^i$ and hence we can conclude that $O(1/\d)$ invocations of one-dimensional binary search are needed to get an approximation of quality $\d$.

Unfortunately, the effectiveness of the method is reduced in higher dimension as the number (and weight) of the  incomparable cones grows. Each rectangle that we process spawns $2^d-2$ recursive sub-computations for each of those cones. This problem can perhaps be tackled using a smaller number of non-elementary overlapping cones as discussed in Section~\ref{sec:dim}. The main problem is that the reduction coefficient for the volume of the uncertainty space after adding the forward and backward cones to the approximation is  $(2^d-2)/2^d$, and tends to one as $d$ grows. Whether or not we should be worried about it depends on whether there are interesting and genuine applications in very high dimension.

Concerning the implementation let us first note all rectangles are disjoint as they are created hierarchically and all elementary cones associated with a point are mutually disjoint. The volume of each rectangle is trivial to compute and hence we can maintain the cumulative volume of all elements of $L$ and stop the algorithm when it goes below the desired approximation error. As for heuristics, we can maintain $L$ sorted in a decreasing size (diameter, volume)  order and always pick the largest rectangle and proceed faster in the reduction of the uncertainty space, although breadth-first processing might give better coverage/diversity.  All these ideas need to be subject to implementation and empirical studies.

The essence of the algorithm is that we treat one rectangle until completion, that is, until $b-a<\epsilon$, than proceed to process its remaining incomparable cones. To see if a more complicated alternative makes sense, let us look closer at what is going on implicitly during the execution of the algorithm, as shown in Figure~\ref{fig:gradual}. Assume that we add the cones associated with $a$ and $b$ to the approximation not only at the end of the search but at each step. Then, as the uncertainty interval along the diagonal gets tighter, the cones in the approximation are replaced by larger and larger cones. Consequently, the approximation of the boundary point on the diagonal by $q=(a+b)/2$ improves and the rectangles incomparable to $q$ become more faithful representatives of the uncertainty space. In the current algorithm they are treated only after the search along the diagonal of the current rectangle terminates according to $\epsilon$. Maybe some other variants of the algorithm can abandon this rectangle earlier, start exploring (some of) the incomparable cones and then maybe even return to the abandoned rectangle later (I am not sure this last feature makes sense). %Figure~\ref{fig:uncert-error} decomposes the volume of the uncertainty space into two parts (under construction).

%\begin{figure}
%  \centering
%    \input gradual.pdftex_t
%  \caption{Adding to the approximating larger and larger cones as the one dimensional search progresses, and making the incomparable cones of $q$ more faithful and having more weight in the volume of the uncertainty set.}\label{fig:gradual}
%\end{figure}
%

\comment{
\begin{figure}
  \centering
    \input uncert-error.pdftex_t
  \caption{The contribution of the the distance from $a$ to $b$ to the volume of the uncertainty space (compared to the contribution of the cones incomparable to $p$)}\label{fig:uncert-error}
\end{figure}
}

\section{Additional Investigations}

We need now to explore how the algorithm works for partition boundaries of different shapes. Figure~\ref{fig:multi-dim3} shows how the algorithm works on a less balanced boundary, very steep in one dimension. The reduction in volume seems to be better than for a balanced boundary but if we look at the alternative quality measure, the length of the box, we see that there will always remains a flat rectangle below with a large width. Perhaps something can be done about it if we add additional assumption on the curvature of the boundary. [to be continued].

%\begin{figure}
%  \centering
%  \input multi-dim3.PDFTEX_T
%  \caption{First steps in approximation a front which is steep in one dimension.}\label{fig:multi-dim3}
%\end{figure}
%

\section{A Fixed-Grid Alternative}
%\clearpage

Let us consider now an alternative based on the more commonly used concept of binary search, $k$-$d$-tress etc. Observe first that a rectangle intersects the boundary between $\uX$ and $\oX$ only if its minimal and maximal points satisfy $\ux\in \uX$ and $\ox\in \oX$. Other rectangles  can be safely moved to $\uY$ or $\oY$. Let $(P_1,P_2)=split(P,i)$  indicate cutting $P$ in the middle along dimension $i$ to obtain two rectangles. Algorithm~\ref{alg:fixed} approximates the partition by unions of rectangles taken from a fixed grid whose resolution is $2^{-j}$ for some $j$. Figure~\ref{fig:quad-tree} illustrates the behavior of the algorithm. The comparison with Figure~\ref{fig:multi-dim1} is a bit misleading because every point there corresponds to $\log(1/\epsilon)$ queries, yet I believe that the diagonal-based algorithm is better.

Note that rectangles added at different stages can be merged into larger rectangles. This is easy to implement by representing elements of $\uY$ by their maximal corner and those in $\oY$ by their minimal corner. Then running a Pareto filter after every step will discard cones which are dominated by/included in other cones.


 \begin{algorithm}
  \caption{Approximating a monotone partition by fixed grid rectangles. }
  \label{alg:fixed}
  \begin{algorithmic}[1]
    \State {{\bf Input}: A rectangle $X$ and a partition $(\uX,\oX)$ }
     \State {{\bf Output}: An approximation $(\uY,\oY)$ by finite unions of rectangular cones}
     \smallskip
     \State{ $L=\{X\}$; $(\uY,\oY)=(\emptyset,\emptyset)$}
    \Repeat
        \State {{\bf pick} $P\in L$; $L=L-\{P\}$  }
        \State {$(P_1,P_2)=split(P,i)$ where $i$ is one of the largest dimensions of $P$}
        \If {$P_1\subset \uX$}
        \State {$\uY=\uY\cup P_1$}
        \Else
        \State {$L=L\cup P_1$}
        \EndIf
           \If {$P_2\subset \oX$}
        \State {$\oY=\oY\cup P_2$}
        \Else
        \State{ $L=L\cup P_2$}
        \EndIf
             \Until {happy}
  \end{algorithmic}
\end{algorithm}

%\begin{figure}[h]
%  \centering
%  \input quad-tree.PDFTEX_T
%  \caption{Approximation of the partition by rectangle taken from a fixed grid.}\label{fig:quad-tree}
%\end{figure}

%\newpage

\section{Fighting the Curse of Dimensionality}
\label{sec:dim}

In order to treat high dimension we can replace the elementary cones by cones of rank $2$ whose number is $2d(d-1)$, which is less than $2^d$ when $d\geq 6$. Such rectangles are based, so to speak, on minimal witnesses for incomparability, a pair of dimensions $(i,j)$ such that $x_i < x'_i$ and $x_j>x'_j$. Two such cones $(i,j)$ and $(i',j')$ are of course overlapping unless $i=j' \wedge j=i'$ and the effect of this fact on the behavior of the algorithm is an empirical question.
Figure~\ref{fig:multi-dim2} illustrates the effect of using overlapping rectangles,  the cones $B_{0\bot}$, $B_{1\bot}$, $B_{\bot 0}$ and $B_{\bot 1}$ of rank $1$. At a first glance it seems that using such cones produces less balanced coverage of the boundary, due to intersections of diagonals.

%\begin{figure}
%  \centering
%  \input multi-dim2.PDFTEX_T
%  \caption{The effect of using overlapping rectangular cones of rank $1$, that is, rectangles characterized by $x_1<q_1$, $x_1>q_1$, $x_2<q_2$ and $x_2>q_2$. }\label{fig:multi-dim2}
%\end{figure}
\clearpage


A point in $x\in X \se  \mathbb{R}^n$  splits the box $X$ into three qualitative subsets, dominated (back cone), dominating (forward cone) and incomparable (the rest): $$
X= B^-(x)\cup B^+(x) \cup I(x).$$ There are always two rectangular cones in any dimension but their non-convex complement can be decomposed into a union of boxes in different (non-canonical)  ways which grows severely with dimension. The relative position of a point $y$ wrt $x$ can be characterized as a branch in a binary tree where at level $i$ we branch left or right according to whether $y_i<x_i$ or $y_i>x_i$. For $n=1$ we have $0$ (smaller) and $1$ (larger). For $n=2$ we have $00$ (smaller), $11$ (larger) and two incomparable boxes $01$ and $10$.  For $n=3$ we have $000$ (smaller), $111$ (larger) and six incomparable boxes, some of which can be merged into larger boxes. The number grows too fast $2^n -2$ so let's consider boxes where all but two dimensions are considered don't care and two other dimensions should have opposite signs, e.g.\ $01*$ or $0*1$ (which have non-empty intersections). This gives a canonical way to write the incomparable sub-space as union of $2 n (n-1)$ rectangles.

There are always two rectangular cones in any dimension but their non-convex complement can be decomposed into a union of boxes in different (non-canonical)  ways which grows severely with dimension. The relative position of a point $y$ wrt $x$ can be characterized as a branch in a binary tree where at level $i$ we branch left or right according to whether $y_i<x_i$ or $y_i>x_i$. For $d=1$ we have $0$ (smaller) and $1$ (larger). For $d=2$ we have $00$ (smaller), $11$ (larger) and two incomparable boxes $01$ and $10$.  For $d=3$ we have $000$ (smaller), $111$ (larger) and six incomparable boxes, some of which can be merged into larger boxes. The number grows too fast $2^d -2$ so let's consider boxes where all but two dimensions are considered don't care and two other dimensions should have opposite signs, e.g.\ $01*$ or $0*1$ (which have non-empty intersections). This gives a canonical way to write the incomparable sub-space as union of $2 d (d-1)$ rectangles. Whether this behaves better than the straightforward decomposition is an empirical question.

\end{document}
