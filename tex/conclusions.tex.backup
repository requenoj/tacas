\section{Conclusions}

%Having presented the algorithm, what remains is to evaluate its performance, both empirically and theoretically. The algorithm has been implemented by Marcell Vazquez-Chanlatte, and it will hopefully be used in the future for systematic evaluation. Preliminary ideas on worst-case complexity were proposed by Nicolas Basset and Eugene Asarin. Other useful comments were made by Alexey Bakhirkin and Dogan Ulus. 

In this paper, we have presented an algorithm for learning the boundary (i.e., Pareto front) between an upward-closed set $\oX$ and its downward-closed complement.
The algorithm is based on an external oracle that answers membership queries $x\in \oX$.
According to the answers and relying on monotonicity, it constructs an approximation of the boundary. 
The algorithm generalizes binary search on the continuum from one-dimensional (and linearly-ordered) domains to multi-dimensional (and partially-ordered) ones.  
To this end, our algorithm divides the multi-dimensional space into multiple smaller blocks that are exhaustively explored until a certain number of points of the Pareto front are approximated.
Our procedure takes into account the characteristics of multi-dimensional spaces for efficiently guiding the discovery of Pareto points. %in a intelligent way.

Besides, the procedure explained in this paper has been implemented in a free and publicly available Python library.
It has been tested with several examples of varying dimension; for instances, checking the membership of points to an hypersphere or a simplex from dimension 2 to 4.
Our algorithm offers good accuracy result within a reasonable amount of time and using affordable hardware resources in small and medium size problems.
The performance of the algorithm seems to be bounded by the time spent by the oracle for solving membership queries.

Future work....