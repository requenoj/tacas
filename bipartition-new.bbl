\begin{thebibliography}{1}

\bibitem{parametric}
Eugene Asarin, Alexandre Donz{\'{e}}, Oded Maler, and Dejan Nickovic.
\newblock Parametric identification of temporal properties.
\newblock In {\em RV}, pages 147--160, 2011.

\bibitem{paretolib}
Nicolas Basset, Oded Maler, and Jos\'e-Ignacio~Requeno Jarabo.
\newblock {P}areto{L}ib library.
\newblock
  \url{https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/tempo/multidimensional_search},
  2018.

\bibitem{ndtree}
Andrzej Jaszkiewicz and Thibaut Lust.
\newblock Nd-tree-based update: a fast algorithm for the dynamic non-dominance
  problem.
\newblock {\em IEEE Transactions on Evolutionary Computation}, 2018.

\bibitem{julien}
Julien Legriel, Colas~Le Guernic, Scott Cotton, and Oded Maler.
\newblock Approximating the {P}areto front of multi-criteria optimization
  problems.
\newblock In {\em {TACAS}}, pages 69--83, 2010.

\bibitem{stl}
Oded Maler and Dejan Nickovic.
\newblock Monitoring temporal properties of continuous signals.
\newblock In {\em Formal Techniques, Modelling and Analysis of Timed and
  Fault-Tolerant Systems}, pages 152--166. Springer, 2004.

\bibitem{amt}
Dejan Ni{\v{c}}kovi{\'c}, Olivier Lebeltel, Oded Maler, Thomas Ferr{\`e}re, and
  Dogan Ulus.
\newblock Amt 2.0: Qualitative and quantitative trace analysis with extended
  signal temporal logic.
\newblock In {\em International Conference on Tools and Algorithms for the
  Construction and Analysis of Systems}, pages 303--319. Springer, 2018.

\bibitem{vazquez_github}
Marcell Vazquez-Chanlatte.
\newblock Multidimensional thresholds.
\newblock \url{https://github.com/mvcisback/multidim-threshold}, 2018.

\bibitem{DBLP:conf/cav/Vazquez-Chanlatte17}
Marcell Vazquez{-}Chanlatte, Jyotirmoy~V. Deshmukh, Xiaoqing Jin, and Sanjit~A.
  Seshia.
\newblock Logical clustering and learning for time-series data.
\newblock In Rupak Majumdar and Viktor Kuncak, editors, {\em Computer Aided
  Verification - 29th International Conference, {CAV} 2017, Heidelberg,
  Germany, July 24-28, 2017, Proceedings, Part {I}}, volume 10426 of {\em
  Lecture Notes in Computer Science}, pages 305--325. Springer, 2017.

\bibitem{DBLP:conf/rv/Vazquez-Chanlatte18}
Marcell Vazquez{-}Chanlatte, Shromona Ghosh, Jyotirmoy~V. Deshmukh, Alberto~L.
  Sangiovanni{-}Vincentelli, and Sanjit~A. Seshia.
\newblock Time-series learning using monotonic logical properties.
\newblock In Christian Colombo and Martin Leucker, editors, {\em Runtime
  Verification - 18th International Conference, {RV} 2018, Limassol, Cyprus,
  November 10-13, 2018, Proceedings}, volume 11237 of {\em Lecture Notes in
  Computer Science}, pages 389--405. Springer, 2018.

\end{thebibliography}
